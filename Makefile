.PHONY: build
build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app -v ./cmd/app

.PHONY: test
test:
	go test -v ./... -race -cover

.PHONY: lint
lint:
	golangci-lint run

.PHONY: run
run:
	go run ./cmd/app/main.go
.DEFAULT_GOAL := run