package main

import (
	"gitlab.com/xamops/healthOnePage/internal/app"
	"os"
)

const (
	helloCatsURL = "https://cataas.com/cat/says/hello?type=square"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	//catUrl := os.Getenv("CAT_URL")
	//if catUrl == "" {
	//	catUrl = helloCatsURL
	//}
	//
	//c := cat.New(context.Background(), catUrl)

	if err := app.New(port).Run(); err != nil {
		return
	}
}
