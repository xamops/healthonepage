FROM golang:1.22 AS builder
LABEL maintainer="Stepan K. <xamust@gmail.com>"
WORKDIR /app
COPY . .
RUN make build

FROM golang:1.22
WORKDIR /app
COPY --from=builder /app .
ENTRYPOINT ["./app"]
EXPOSE 8080