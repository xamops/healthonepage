package cat

import (
	"context"
	"fmt"
	"io"
	"net/http"
)

type HelloCats struct {
	client *http.Client
	url    string
}

func New(ctx context.Context, url string) *HelloCats {
	client := &http.Client{}
	return &HelloCats{
		client: client,
		url:    url,
	}
}

func (h *HelloCats) GetHelloCatURL() (string, error) {
	get, err := h.client.Get(h.url)
	if err != nil {
		return "", fmt.Errorf("failed to get hello cat: %w", err)
	}
	defer get.Body.Close()

	body, err := io.ReadAll(get.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body hello cat: %w", err)
	}
	return string(body), nil
}
