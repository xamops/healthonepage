package app

import "gitlab.com/xamops/healthOnePage/internal/app/http"

type App struct {
	Port string
}

func New(port string) *App {
	return &App{
		Port: port,
	}
}

func (a *App) Run() error {
	return http.New(a.Port)
}
