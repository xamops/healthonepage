package http

import "net/http"

func getIpMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ip := r.Header
		ip.Add("remote_addr", r.RemoteAddr)
		h.ServeHTTP(w,
			r.WithContext(
				setIP(r.Context(), ip),
			),
		)
	})
}
