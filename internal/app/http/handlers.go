package http

import (
	"fmt"
	_ "golang.org/x/exp/maps"
	"log"
	"net/http"
	"strings"
	"time"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ip, _ := getIP(ctx)
	log.Println("Check!")
	log.Println(ip)

	w.Write([]byte(strings.Join([]string{
		"<h1>Are you OK?</h1>",
		"<img src=\"https://cataas.com/cat/says/hello?type=square\">",
		fmt.Sprintf(
			"<h1>%v</h1>",
			time.Now().Format(time.RFC3339)),
		fmt.Sprintf("%#v", ip)},
		"")))
}
