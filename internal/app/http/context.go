package http

import "context"

type ctxKey string

const ctxUserIP = ctxKey("userip")

func setIP(ctx context.Context, ip map[string][]string) context.Context {
	return context.WithValue(ctx, ctxUserIP, ip)
}
func getIP(ctx context.Context) (ip map[string][]string, ok bool) {
	ip, ok = ctx.Value(ctxUserIP).(map[string][]string)
	return
}
