package http

import (
	"log"
	"net/http"
)

type App struct {
}

func New(port string) error {
	mux := http.NewServeMux()

	mux.Handle("/health", getIpMiddleware(http.HandlerFunc(indexHandler)))

	log.Println("Start")
	return http.ListenAndServe(":"+port, mux)
}
